# WebDL_fu #

I think this is mainly of interest to people who want to modify the existing delx/webdl project.  
webdl_fu is a set of Python (ver. 3) scripts that form a wrapper for the delx/webdl project: https://bitbucket.org/delx/webdl.

I like using the delx/webdl software but would like to alter the interface to suit my use of the software without impacting the delx/webdl source code.

I would also like to take advantage of delx/webdl source updates with as little fuss as possible. To that end, the wrapper files (all the files ending with **_fu**) contain the tweaks I have added. The original delx/webdl software is unchanged in webdl_fu so future delx/webdl software updates can be easily managed by webdl_fu.

The tweaks are implemented for ABC and Iview. I haven't changed Ten as I rarely watch the station. If someone is interested in tweaking the Ten module then contact me and I will look into it.

It is mainly of interest to people who use **autograbber.py**
To use **webdl_fu**; use **autograbber_fu.py**
The operation of **autograbber.py** is not altered by the installation or use of **autograbber_fu.py**


## Installation
**fordute37/webdl_fu** only uses standard packages supplied with Python3 (e.g.: pathlib, re) so the installation follows after the installation of delx/webdl.  
1. Follow the instructions supplied by delx/webdl for the installation of the webdl software.  
2. Clone the webdl_fu repository: **git clone git@bitbucket.org:fordute37/webdl_fu.git**  
3. Copy the python files from the webdl_fu directory into the webdl directory

Use **autograbber_fu.py** instead of **autograbber.py**

Current wrapper files:  
**autograbber_fu.py**  
**common_fu.py**  
**common_fu.pyc**  
**iview_fu.py**  
**sbs_fu.py**  


## What the wrapper does.
The purpose of webdl is to implement interface tweaks that are not currently available in webdl.  
It also provides a template for creating wrappers for delx/webdl so other users can apply personal updates not available through the original source.

### Download captions
webdl_fu downloads captions from SBS and Iview if they are available.

### Fill History
Sometimes I want to clean out the **history file** (.history.txt) and refresh it with currently available titles or just update the list without downloading anything. Inserting a **special title** into the history file will cause new titles to be inserted into the history file witthout downloading the video. In the current implementation the special title is: **0FILLHISTORY0**

If this string appears on a line by itself then no videos will be downloaded and any new titles will be added to the history file.

### Ignore titles
If there are titles that you don't want downloaded then they can be *ignored* by adding an **ignore pattern** to the **history file** (.history.txt).  
The pattern must match from the beginning of the line/title.  
The ignore pattern template is: **0ignore_pattern0**.  
The **ignore_pattern** can be any valid python regular expression.  
The first and last '0' characters are stripped from the string when matching the title.

examples:    
Title: **A New TV Drama**  
Seasons available: 2, 3, 4  

History file entries:  
0A New TV Drama0    # The series will not be downloaded  
0New TV Drama0      # Series is downloaded because the pattern does not match from the beginning of the line/title  
0A New TV Drama S[23]0         # SBS. Do not download series 2 or 3  
0A New TV Drama Series [23]0   # ABC. Do not download series 2 or 3  
0A New S\d+0                   # SBS. Download A New TV Drama. Do not download any of series "A New"  
0.\*\(English\).\*0            # Do not download anything with "(English)" in the title  

### Video File naming
The files for TV series downloaded by delx/WebDL are named according to video titles supplied by the data sources. In 
fordute37/webdl_fu the downloaded filenames are standardized to the use **S##E##** instead of the current **S# Ep#** or **Series # Ep #** 

### Specify download quality for streamlink
For videos downloaded by streamlink it is possible to specify the preferred stream to download. The downloaded stream is selected using the **SLNK_HLS_QUAL** variable specified in the files *iview_fu.py* and *sbs_fu.py* files. Alter the value of these variables to select your preferred stream. The format of the value must conform to the *streamlink* guidelines.

The following is copied from *streamlink -h*:
    STREAM
    Stream to play.
    
    Use "best" or "worst" for selecting the highest or lowest available quality.
    
    Fallback streams can be specified by using a comma-separated list:
    
      "720p,480p,best"
    
    If no stream is specified and --default-stream is not used, then a
    list of available streams will be printed.

### run_autograbber.sh
Another way to manually run autograbber

