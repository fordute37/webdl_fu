from common import grab_json, grab_xml, Node, sanify_filename
from common import append_to_qs
from common_fu import download_hls

import requests_cache
import urllib.parse
import re

from pprint import pprint
from pathlib import Path

# _fu
from iview import API_URL #, AUTH_URL

from iview import  IviewEpisodeNode, IviewIndexNode, IviewSeriesNode
from iview import  IviewFlatNode, IviewRootNode
from iview import  format_episode_title

SLNK_HLS_QUAL = '576p, best'
SLNK_HLS_QUAL = None

def add_episode(parent, ep_info):

    video_key = ep_info["episodeHouseNumber"]
    series_title = ep_info["seriesTitle"]
    title = ep_info.get("title", None)
    episode_title = format_episode_title(series_title, title)

    IviewEpisodeNode_fu(episode_title, parent, video_key)

class IviewEpisodeNode_fu(IviewEpisodeNode):

    def format_output_filename(self):
        r""" tweak the output filename 
        SBS title format: 
        title S\d+ Ep\d+
            or
        title S\d+ Ep\d+ - Episode Title
        """
        ep = self.filename
        f_ext = Path(ep).suffix.lower()
        stem = Path(ep).stem    
        if '/' in ep:
            stem = ep.replace(f_ext, '')

        patterns_m3 = []
        patterns_m3.append(r'(.*?)\s+Series\s+(\d+)\s+Ep\s*(\d+)$')
        patterns_m4 = []
        patterns_m4.append(r'(.*?)\s+Series\s+(\d+)\s+Ep\s*(\d+)\s+(.+)$')
        patterns_PA = []  # planet america
        patterns_PA.append(r'(Planet America.*)\s+(\d+)/(\d+)/(\d+)$')
        patterns_SM = []  # Shaun Micallef specials
        patterns_SM.append(r'(Shaun Micallef[\S]*)\s+(.*)$')

        for p in patterns_m3:
            d = re.match(p, stem.strip())
            if d:
                stem = "{0}.S{1:02d}E{2:02d}".format(d.group(1), int(d.group(2)), int(d.group(3)))
                return(stem + f_ext)

        for p in patterns_m4:
            d = re.match(p, stem.strip())
            if d:
                stem = "{0}.S{1:02d}E{2:02d}.{3}".format(d.group(1).strip(), int(d.group(2)), int(d.group(3)), d.group(4).strip())
                return(stem + f_ext)
        
        for p in patterns_PA:
            d = re.match(p, stem.strip())
            if d:
                stem = "{0}.({1:04d}).S{2:02d}E{3:02d}".format(d.group(1).replace(r"'",'').strip(), int(d.group(4)), int(d.group(3)), int(d.group(2)))
                return(stem + f_ext)

        for p in patterns_SM:
            d = re.match(p, stem.strip())
            if d:
                stem = "{0}.{1}".format(d.group(1).replace(r"'",'').strip(), d.group(2).strip())
                return(stem + f_ext)
         
        print("{}: No match found for string ({})".format('sbs_fu:format_output_filename', ep.strip()))
        return(ep)

    def download(self):
        info = grab_json(API_URL + "/programs/" + self.video_key)
        if "playlist" not in info:
            return False
        video_url = self.find_hls_url(info["playlist"])
        # token, token_hostname= self.get_auth_details()
        # video_url = self.add_auth_token_to_url(video_url, token, token_hostname)
        auth_token = self.get_auth_token()
        video_url = append_to_qs(video_url, {"hdnea": auth_token})
        filename = self.format_output_filename()
        return download_hls(filename, video_url, SLNK_HLS_QUAL)

    def find_captions_url(self, playlist):
        for video in playlist:
            if video["type"] == "program":
                return video["captions"]['src-vtt']
        raise Exception("INFO: No captions available for " + self.video_key)


    def download_captions(self):
        info = grab_json(API_URL + "/programs/" + self.video_key)
        captions_url = self.find_captions_url(info["playlist"])
        
        
        if captions_url is not None:
            captions_ext = Path(captions_url).suffix.lower()
            captions_filename = self.format_output_filename()
            captions_filename = captions_filename.replace('.ts', captions_ext)
            return self.download_vtt(captions_filename, captions_url)
        return

    def download_vtt(self, filename_vtt, captions_url):
        filename = sanify_filename(filename_vtt)
        print("Downloading: %s (%s)" % (filename, captions_url))
        response = urllib.request.urlopen(captions_url)
        data = response.read()
        response.close()

        file_ = open(filename, 'wb')
        file_.write(data)
        file_.close()
        return 1

class IviewSeriesNode_fu(IviewSeriesNode):

    def fill_children(self):
        ep_info = grab_json(self.url)
        series_slug = ep_info["href"].split("/")[1]
        series_url = API_URL + "/series/" + series_slug + "/" + ep_info["seriesHouseNumber"]
        info = grab_json(series_url)
        for ep_info in info.get("episodes", []):
            add_episode(self, ep_info)

class IviewIndexNode_fu(IviewIndexNode):

     def add_series(self, ep_info):
        title = ep_info["seriesTitle"]
        if title in self.unique_series:
            return
        self.unique_series.add(title)
        url = API_URL + "/" + ep_info["href"]
        IviewSeriesNode_fu(title, self, url)   

class IviewFlatNode_fu(IviewFlatNode):
    pass


class IviewRootNode_fu(IviewRootNode):
    def load_categories(self):
        by_category_node = Node("By Category", self)
        def category(name, slug):
            IviewIndexNode_fu(name, by_category_node, API_URL + "/category/" + slug)

        category("Arts & Culture", "arts")
        category("Comedy", "comedy")
        category("Documentary", "docs")
        category("Drama", "drama")
        category("Education", "education")
        category("Lifestyle", "lifestyle")
        category("News & Current Affairs", "news")
        category("Panel & Discussion", "panel")
        category("Regional Australia", "regional")
        category("Sport", "sport")

    def load_channels(self):
        by_channel_node = Node("By Channel", self)
        def channel(name, slug):
            IviewIndexNode_fu(name, by_channel_node, API_URL + "/channel/" + slug)

        channel("ABC1", "abc1")
        channel("ABC2", "abc2")
        channel("ABC3", "abc3")
        channel("ABC4Kids", "abc4kids")
        channel("News", "news")
        channel("ABC Arts", "abcarts")
        channel("iView Exclusives", "iview")

    def load_featured(self):
        IviewFlatNode("Featured", self, API_URL + "/featured")

    def fill_children(self):
        self.load_categories()
        self.load_channels()
        self.load_featured()

def fill_nodes(root_node):
    IviewRootNode_fu("ABC iView", root_node)