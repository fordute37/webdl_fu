import hashlib
import io
import json
import logging
import lxml.etree
import lxml.html
import os
import re
import requests
import requests_cache
import shutil
import signal
import subprocess
import time
import urllib.parse


try:
    import autosocks
    autosocks.try_autosocks()
except ImportError:
    pass


logging.basicConfig(
    format = "%(levelname)s %(message)s",
    level = logging.INFO if os.environ.get("DEBUG", None) is None else logging.DEBUG,
)
  
CACHE_FILE = os.path.join(
    os.environ.get("XDG_CACHE_HOME", os.path.expanduser("~/.cache")),
    "webdl",
    "requests_cache"
)
if not os.path.isdir(os.path.dirname(CACHE_FILE)):
    os.makedirs(os.path.dirname(CACHE_FILE))

requests_cache.install_cache(CACHE_FILE, backend='sqlite', expire_after=3600)

from common import Node, sanify_filename, ensure_scheme
from common import grab_text, grab_html, grab_xml, grab_json
from common import exec_subprocess, check_command_exists, find_ffmpeg
from common import find_ffprobe, get_duration # , find_streamlink
from common import check_video_durations, remux, convert_to_mp4
from common import download_hds, download_mpd
from common import download_http, natural_sort, append_to_qs

def load_root_node():
    root_node = Node("Root")

    import iview_fu
    iview_fu.fill_nodes(root_node)

    import sbs_fu
    sbs_fu.fill_nodes(root_node)

    import ten
    ten.fill_nodes(root_node)

    return root_node

def download_hls(filename, video_url, stream_quality="best"):
    """
    add stream quality to parameters. 
    defaults to "best" (hard-coded value in webdl)
    from streamlink -h
    ***
    STREAM
    Stream to play.
    
    Use "best" or "worst" for selecting the highest or lowest available quality.
    
    Fallback streams can be specified by using a comma-separated list:
    
      "720p,480p,best"
    
    If no stream is specified and --default-stream is not used, then a
    list of available streams will be printed.
    ***
    """
    streamlink = "streamlink" # find_streamlink()

    if stream_quality is None or stream_quality == '':
        stream_quality = 'best'

    filename = sanify_filename(filename)
    video_url = "hlsvariant://" + video_url
    logging.info("Downloading: %s", filename)

    cmd = [
        streamlink,
        "-f",
        "-o", filename,
        video_url,
        stream_quality,
    ]
    if exec_subprocess(cmd):
        return convert_to_mp4(filename)
    else:
        return False