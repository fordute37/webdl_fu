import requests_cache
from common import grab_html, grab_json, grab_xml, download_mpd, Node, append_to_qs
from common import sanify_filename 
from common_fu import download_hls
import urllib, pathlib

import json
import sys
import re
import itertools

# _fu
from sbs import BASE, FULL_VIDEO_LIST, VIDEO_URL, NS, PARAMS_URL
from sbs import SbsVideoNode, SbsNavNode
SLNK_HLS_QUAL = '720p, best'
#SLNK_HLS_QUAL = '360p, best'
# SLNK_HLS_QUAL = '720p'

class SbsVideoNode_fu(SbsVideoNode):

    def __init__(self, title, parent, url):
        super().__init__(title, parent, url)
        # m = re.match(r'The .*', self.title)
        # if m is not None:
        #     print("this is {}".format(self.title))
        #     pass
            

    def format_output_filename(self):
        r""" tweak the output filename 
        SBS title format: 
        title S\d+ Ep\d+
            or
        title S\d+ Ep\d+ - Episode Title
        """
        ep = self.title
        patterns_m3 = []
        patterns_m3.append(r'(.*?)\s+S(\d+)\s+Ep(\d+)$')
        patterns_m4 = []
        patterns_m4.append(r'(.*?)\s+S(\d+)\s+Ep(\d+)[\ \-]+(.+)$')

        for p in patterns_m3:
            d = re.match(p, ep.strip())
            if d:
                ep = "{0}.S{1:02d}E{2:02d}".format(d.group(1), int(d.group(2)), int(d.group(3)))
                return(ep)

        for p in patterns_m4:
            d = re.match(p, ep.strip())
            if d:
                ep = "{0}.S{1:02d}E{2:02d}.{3}".format(d.group(1).strip(), int(d.group(2)), int(d.group(3)), d.group(4).strip())
                return(ep)
        
        print("{}: No match found for string ({})".format('sbs_fu:format_output_filename', ep.strip()))

        return(ep)

    def download(self):
        with requests_cache.disabled():
            doc = grab_html(VIDEO_URL % self.video_id)
        # player_params = self.get_player_params(doc)
        player_params = grab_json(PARAMS_URL % self.video_id) # see https://bitbucket.org/delx/webdl/issues/119/sbs-not-finding-player-params
        release_url = player_params["releaseUrls"]["html"]
        if release_url == '':
            raise Exception("release URL is empty")

        self.description = player_params["description"]
        filename = self.title + ".ts"
        filename = self.format_output_filename() + ".ts"

        hls_url = self.get_hls_url(release_url)
        if hls_url:
            return download_hls(filename, hls_url, SLNK_HLS_QUAL)
        else:
            return download_mpd(filename, release_url)

    def download_srt(self, filename_srt, captions_url):
        print("download_captions:(%s)\n" % filename_srt)
        filename = sanify_filename(filename_srt)
        response = urllib.request.urlopen(captions_url)
        data = response.read()
        response.close()

        file_ = open(filename, 'wb')
        file_.write(data)
        file_.close()
        return 1
        
    def get_captions_url(self, release_url):
        with requests_cache.disabled(): 
            doc = grab_xml("http:" + release_url.replace("http:", "").replace("https:", ""))
            captions = doc.xpath("//smil:textstream", namespaces=NS)

            if not captions:
                return 

            i_srt = 0
            i = 0
            for cf in captions:
                f_ext = pathlib.Path(cf.attrib['src']).suffix.lower()
                if(f_ext == '.srt'):
                    i_srt = i
                    break
                i += 1
                
            captions_url = captions[i_srt].attrib["src"]
        return captions_url

    def download_captions(self):
        with requests_cache.disabled():
            doc = grab_html(VIDEO_URL % self.video_id)
            # player_params = self.get_player_params(doc)
            player_params = grab_json(PARAMS_URL % self.video_id) # see https://bitbucket.org/delx/webdl/issues/119/sbs-not-finding-player-params

            release_url = player_params["releaseUrls"]["html"]
            if release_url == '':
                raise ZeroDivisionError('Invalid release url')

            captions_url = self.get_captions_url(release_url)
            if not captions_url:
                print("No captions supplied %s: %s" % (self.video_id, self.title))
                return None
            
            f_ext = pathlib.Path(captions_url).suffix.lower()
            filename = self.title + f_ext
            filename = self.format_output_filename() + f_ext
            ret = self.download_srt(filename, captions_url)
        return ret

class SbsNavNode_fu(SbsNavNode):
    def create_video_node(self, entry_data):
        SbsVideoNode_fu(entry_data["title"], self, entry_data["id"])

class SbsRootNode_fu(SbsNavNode_fu):
    """
    Copy contents of sbs.SbsRootNode because it inherits SbsNavNode
    """
    def __init__(self, parent):
        Node.__init__(self, "SBS", parent)

    def fill_children(self):
        all_video_entries = self.load_all_video_entries()
        category_and_entry_data = self.explode_videos_to_unique_categories(all_video_entries)
        for category_path, entry_data in category_and_entry_data:
            nav_node = self.create_nav_node(self, category_path)
            nav_node.create_video_node(entry_data)
        pass

    def load_all_video_entries(self):
        offset = 1
        amount = 50
        uniq = set()
        while True and offset < 10000:
            # print("Offset = {}".format(str(offset)))
            url = append_to_qs(FULL_VIDEO_LIST, {"range": "%s-%s" % (offset, offset+amount-1)})
            data = grab_json(url)
            if "entries" not in data:
                # raise Exception("Missing data in SBS response", data)
                print("\nERRORERRORERRORERROR\n{}\n{}\n\n".format("Missing data in SBS response", str(data)))
            entries = data["entries"]
            if len(entries) == 0:
                break
            for i, entry in enumerate(entries):
                if ("guid" in entry):
                    if (entry["guid"] not in uniq):
                        uniq.add(entry["guid"])
                        yield entry
                else:
                    print("{}.{}: {}. Missing GUID for {}".format("sbs_fy.py", "load_all_video_entries", i, entry["title"]))
                    yield entry
            offset += amount
            sys.stdout.write(".")
            sys.stdout.flush()
        print()

    def explode_videos_to_unique_categories(self, all_video_entries):
        for entry_data in all_video_entries:
            for category_data in entry_data["media$categories"]:
                category_path = self.calculate_category_path(
                    category_data["media$scheme"],
                    category_data["media$name"],
                )
                if category_path:
                    yield category_path, entry_data

    def calculate_category_path(self, scheme, name):
        if not scheme:
            return
        if scheme == name:
            return
        name = name.split("/")
        if name[0] != scheme:
            name.insert(0, scheme)
        return name

    def create_nav_node(self, parent, category_path):
        if not category_path:
            return parent

        current_path = category_path[0]
        current_node = parent.find_existing_child(current_path)
        if not current_node:
            current_node = SbsNavNode_fu(current_path, parent)
        return self.create_nav_node(current_node, category_path[1:])

def fill_nodes(root_node):
    SbsRootNode_fu(root_node)

if __name__ == "__main__":

    pass
