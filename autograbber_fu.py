#!/usr/bin/env python3

import os
print("Working directory: {}".format(os.getcwd()))
from common_fu import load_root_node
import common_fu
import fnmatch
import logging
import shutil
import sys

# _fu
import re
from autograbber import DownloadList, check_directories

HISTORY_FILENAME = ".history.txt"
PATTERN_FILENAME = ".patterns.txt"

# _fu
FILL_HISTORY_STRING = "FILLHISTORY"
HISTORY_SEEN_LIST_DELIMITER = '0'

class DownloadList_fu(DownloadList):

    def __init__(self):
        self.seen_list_re = set()
        super().__init__()

    def _load_history_file(self):
        self._move_old_file("downloaded_auto.txt")
        self._move_old_file(".downloaded_auto.txt")

        # match any line beginning with '0' and ending with '*'
        # want the start with '0' so that ignore patterns are grouped
        # when the file is sorted
        ignore_pattern = re.compile(r'^' + HISTORY_SEEN_LIST_DELIMITER + r'(.*)' + HISTORY_SEEN_LIST_DELIMITER + r'$')

        try:
            with open(HISTORY_FILENAME, "r") as f:
                for line in f:
                    self.seen_list.add(line.strip())
                    m = ignore_pattern.match(line.strip())
                    if m:
                        lin = m.group(1)
                        self.seen_list_re.add(lin)
                        print('{}: seen_list_re.add({})'.format('autograbber_fu.py', lin))
        except Exception as e:
            logging.error("_load_history_file: file: %s -- %s", HISTORY_FILENAME, e)

    def has_seen(self, node):
        """
        Changes 20190731.
        if the '0'+FILL_HISTORY_STRING+'0' is in the .history.txt file
        then mark the title as seen in the history file.
        Do not down load the file.
         
        If the title matches a pattern in seen_list_re
        then mark the title as seen in the history file.
        Do not download the file.

        The code strips the final asterisk and does an re.match
        so the match pattern can be a regular expression.
        
        example: SBS series title 'Another Drama' has 4 seasons.
        User only wants season 4. 
        Add the following entries to the .history.txt file:
            Another Drama S1*
            Another Drama S2*
            Another Drama S3*

        example: ABC series title 'Another Drama' has 4 seasons.
        User only wants season 4. 
        Add the following entries to the .history.txt file:
            Another Drama Series 1*
            Another Drama Series 2*
            Another Drama Series 3*
        """
        if node.title.strip() in self.seen_list:
            return True
        for pattern in self.seen_list_re:
            if pattern == FILL_HISTORY_STRING:
                self.mark_seen(node)
                return True
            if re.match(pattern, node.title):
                print('{}.{}: MARK SEEN: {}, match: ({})'.format('autograbber.py', 'has_seen', node.title.strip(), pattern))
                self.mark_seen(node)
                return True
        return False

def match(download_list, node, pattern, count=0):
    if node.can_download:

        if not download_list.has_seen(node):
            try:  # download captions
                pass
                node.download_captions()
            except ZeroDivisionError:
                logging.error("release_url empty for: {}".format(node.title))
                return
            except Exception as e: 
                logging.error("1: Failed to download captions for: {}".format(node.title))
                logging.error(e)
            try:  # download video
                # download_list.mark_seen(node)
                if node.download():
                    download_list.mark_seen(node)
                else:
                    logging.error("Failed to download! {}".format(node.title))
            except Exception as e: 
                # print(e)
                logging.error("2: Failed to download video for:{}".format(node.title))
                logging.error(e)
        return

    if count >= len(pattern):
        logging.error("No match found for pattern: {}".format("/".join(pattern)))
        return
    p = pattern[count]
    for child in node.get_children():
        if fnmatch.fnmatch(child.title, p):
            match(download_list, child, pattern, count+1)

def process_one_dir(destdir, patternfile):
    """ in here to use import from common_fu """
    os.chdir(destdir)
    node = load_root_node()
    download_list = DownloadList_fu()

    for line in open(patternfile):
        search = line.strip().split("/")

        match(download_list, node, search)

def process_dirs(download_dirs):
    """ in here to use local process_one_dir"""
    for download_dir, pattern_filename in check_directories(download_dirs):
        logging.info("Processing directory: %s", download_dir)
        process_one_dir(download_dir, pattern_filename)

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("Usage: %s download_dir [download_dir ...]" % sys.argv[0])
        sys.exit(1)

    if len(sys.argv) == 3 and os.path.isfile(sys.argv[2]):
        # Backwards compatibility with old argument format
        destdir = os.path.abspath(sys.argv[1])
        patternfile = os.path.abspath(sys.argv[2])
        run = lambda: process_one_dir(destdir, patternfile)

    else:
        run = lambda: process_dirs(sys.argv[1:])

    try:
        run()
    except (KeyboardInterrupt, EOFError):
        print("\nExiting...")
